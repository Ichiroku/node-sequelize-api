'use strict';
module.exports = (sequelize, DataTypes) => {
  const User = sequelize.define('User', {
    name: DataTypes.STRING,
    email: DataTypes.STRING,
    password: DataTypes.STRING,
    isActive: DataTypes.BOOLEAN
  }, {});
  
  User.associate = function(models) {
    User.hasOne(models.Role);
  };
  
  User.fetchAll = (active = true) => {
    return User.findAll({ where: {isActive: active} });
  }

  User.findById = (id) => {
    return User.findOne({ where: {id} });
  }
  
  return User;
};