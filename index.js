const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const sequelize = require('./sequelize');

const dotenv = require('dotenv');
dotenv.config({ path: './config.env' });

const DB = require('./models');

//Initialize Express
const app = express();

//Base middlewares for Express
app.use(cors());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

//Default headers
app.use((req, res, next) => {
    res.header("Access-Control-Allow-Origin", "*");
    res.header(
      "Access-Control-Allow-Headers",
      "Origin, X-Requested-With, Content-Type, Accept, Authorization"
    );
  
    if (req.method === "OPTIONS") {
      res.header("Access-Control-Allow-Methods", "PUT, POST, PATCH, DELETE, GET");
      return res.status(200).json({});
    }
  
    next();
});

// Error handling middleware
app.use((err, req, res, next) => {
    console.log(err);
    res.status(err.status || 500)
    res.send({
      message: err.message,
      errors: err.errors,
    })
});

//Test Sequelize connection
const sequelizeConnection = async () => {
    try {
        await sequelize.authenticate();
        const users = await DB.User.fetchAll();
        console.log("Database connection OK");
        console.log('users', users);
    } catch (error) {
        console.error('Unable to connect to DB', error);
    }
}

app.listen(process.env.PORT || 3000, () => {
    console.log(`Server is running on port ${process.env.PORT}`);
    sequelizeConnection();
});